import "./App.css";
import ArtifactList from "./components/ArtifactList";
import Navbar from "./components/Navbar";
import RepositoryList from "./components/RepositoryList";

function App() {
  return (
    <div className="App">
      <div className="Flexbox">
      <Navbar/>
      <RepositoryList />
      </div>
    </div>
  );
}

export default App;
