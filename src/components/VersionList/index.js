import React from "react";
import { useState } from "react";
import GetList from "../../requests/GetList";
import styles from "./index.module.css";
import ArtifactList from "../ArtifactList";

const VersionList = ({ repoName }) => {
  const [data, loading, error] = GetList(`http://localhost:8081/azure-blob-storage/versions/${repoName}`);

  const [selectedVersion, setSelectedVersion] = useState('');

  return (
    <div className={styles.flex}>
      <div className={styles.listContainer}>
      <div className={styles.listTitle}>Versions:</div>
        {loading && <div className={styles.loader}>Loading posts...</div>}
        {error && <div className={styles.error}>{error}</div>}

        <ul>
          {data &&
            data.map((version) => (
              <li className={styles.contentContainer} onClick={() => {setSelectedVersion(version)}}>
                Version: {version}
              </li>
            ))}
        </ul>
      </div>
      <ArtifactList repoName={repoName} version={selectedVersion}/>
    </div>
  );
};

export default VersionList;
