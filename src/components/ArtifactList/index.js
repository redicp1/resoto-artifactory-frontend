import React from "react";
import { useState } from "react";
import GetList from "../../requests/GetList";
import styles from "./index.module.css";

const ArtifactList = ({repoName, version}) => {
  const id = useState(null);

  const [data, loading, error] = GetList(`http://localhost:8081/azure-blob-storage/artifacts/${repoName}/${version}`);

  return (
    <div className={styles.flex}>
      <div className={styles.listContainer}>
      <div className={styles.listTitle}>Artifacts:</div>
        {loading && <div className={styles.loader}>Loading posts...</div>}
        {error && <div className={styles.error}>{error}</div>}

        <ul>
          {data &&
            data.map((artifact) => (
              <li key={id} className={styles.contentContainer}>
                Artifact: {artifact}
              </li>
            ))}
        </ul>
      </div>
    </div>
  );
};

export default ArtifactList;
