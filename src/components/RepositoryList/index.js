import React from "react";
import GetList from "../../requests/GetList";
import styles from "./index.module.css";
import { useState } from "react";
import VersionList from "../VersionList";

const RepositoryList = () => {
  const [data, loading, error] = GetList("http://localhost:8081/azure-blob-storage/repositories");

  const [selectedRepo, setSelectedRepo] = useState('');

  return (
    <div className={styles.flex}>
      <div className={styles.listContainer}>
        <div className={styles.listTitle}>Repositories:</div>
        {loading && <div className={styles.loader}>Loading posts...</div>}
        {error && <div className={styles.error}>{error}</div>}

        <ul>
          {data &&
            data.map((name) => (
              <li className={styles.contentContainer} onClick={() => {setSelectedRepo(name)}}>
                Name: {name}
              </li>
            ))}
        </ul>
      </div>
      <VersionList repoName={selectedRepo}/>
    </div>
  );
};

export default RepositoryList;
