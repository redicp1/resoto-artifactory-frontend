import React from "react";
import { useState } from "react";
import GetList from "../../requests/GetList";
import styles from "./index.module.css";

const ArtifactDetails = () => {
  const id = useState(null);

  const [data, loading, error] = GetList(`http://localhost:3032/artifact/${1}`);

  return (
    <div className={styles.flex}>
      <div className={styles.detailContainer}>
        {loading && <div className={styles.loader}>Loading posts...</div>}
        {error && <div className={styles.error}>{error}</div>}
        {data && (
          <div className={styles.contentContainer}>
            <div>{data?.reponame}</div>
            <div>{data?.repotype}</div>
            <div>{data?.name}</div>
            <div>{data?.version}</div>
            <div>{data?.size}</div>
            <div>{data?.createdDate}</div>
            <div>{data?.owner}</div>
          </div>
        )}
      </div>
    </div>
  );
};

export default ArtifactDetails;
